package com.iviu.gcm;

import java.io.IOException;

public class Main
{

	public static void main(String[] args) throws IOException
	{
		/**
		 * Replace Constants.SERVER_API_KEY with your senders Id and
		 * Constants.RYAN_REGISTRATION_ID with your phone's GCM registration id.
		 * Then just run the project!
		 */
		Sender sender = new Sender(Constants.SERVER_API_KEY);
		Message message = new Message.Builder().addData("message", "Hi Joe")
				.addData("sub", "This is a GCM message").build();
		sender.sendNoRetry(message, Constants.RYAN_REGISTRATION_ID);
	}
}
